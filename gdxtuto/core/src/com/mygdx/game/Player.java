package com.mygdx.game;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
 
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
 
public class Player implements Serializable {
       
        private static final transient String defaultTexture = "retro-mario.png";
       
        private static final long serialVersionUID = 1L;
        
        private static final int col = 4;
        private static final int row = 4;
        // Adding transient to anything upon declaration will cause it to be ignored when deserialized and serialized.
        transient Texture texture;
        Vector2 position;
        Vector2 destinationPosition;
        String textureLoc;
        
        
        Animation animation;
        Texture playerTexture;
        TextureRegion[] frames;
        TextureRegion currentFrame;
        
        

		float stateTime;
        Rectangle bounds;
       
        public Player (Vector2 position, String textureLoc){
                this.position = position;
                this.textureLoc = textureLoc;
               this.destinationPosition = position;
                // Texture CANNOT be instantiated here, why? Because if the file already exists then it will attempt to load it from said file.
                // Therefore the player constructor will never be accessed. Rather then placing it here we create a new method called
                // loadContent().
                // texture = new Texture(Gdx.files.internal(textureLoc));
               
               playerTexture = new Texture(Gdx.files.internal("profwalk.png"));
               TextureRegion[][] tmp = TextureRegion.split(playerTexture, playerTexture.getWidth() / col, playerTexture.getHeight() / row);
               frames = new TextureRegion[col * row];
               int index = 0;
               for(int i = 0; i < row; i++){
                       for(int j = 0; j < col; j++){
                               frames[index++] = tmp[i][j];
                       }
               }
               animation = new Animation(1f, frames);
               stateTime = 0f;
               currentFrame = animation.getKeyFrame(0);
               bounds = new Rectangle(position.x, position.y, currentFrame.getRegionWidth(), currentFrame.getRegionHeight());
        }
       
        // This is so that DemoGame can load the content AFTER the player has been deserialized or created.
        public void loadContent() {
                if (Gdx.files.internal(textureLoc).exists())
                        texture = new Texture(Gdx.files.internal(textureLoc));
                else
                        texture = new Texture(Gdx.files.internal(defaultTexture));
        }
       
        public void update() {
        	stateTime += Gdx.graphics.getDeltaTime();
        	if(stateTime > 4){
        	stateTime = 0;
        	}
        	stateTime += Gdx.graphics.getDeltaTime(); 
        	 if (Gdx.input.isTouched()){
 	            System.out.println("clicked:" + Gdx.input.getX() + ":" +Gdx.input.getY());
 	            destinationPosition = new Vector2(Gdx.input.getX(), Gdx.graphics.getHeight()-Gdx.input.getY());
 	            
 	        }
 	        
 	        if (position.x != destinationPosition.x || position.y != destinationPosition.y){
 	            
 	            if (position.x < destinationPosition.x){
 	               position.x += 1f;
 	               currentFrame = animation.getKeyFrame(12 + stateTime);
 	            }
 	            if (position.y < destinationPosition.y){
 	                position.y += 1f;
 	               
 	              currentFrame = animation.getKeyFrame(8 + stateTime);
 	            }
 	            if (position.x > destinationPosition.x){
 	                position.x -= 1f;
 	               currentFrame = animation.getKeyFrame(4 + stateTime);
 	            }
 	            if (position.y > destinationPosition.y){
 	                position.y -= 1f;
 	               currentFrame = animation.getKeyFrame(0 + stateTime);
 	            }
 	        }
        }
       
        // I took the liberty of implementing a draw, it's up to you whether you want to implement one or just grab
        // the parameters from the player.
        public void draw(SpriteBatch batch) {
                batch.draw(currentFrame, position.x, position.y);
        }
       
        public static void savePlayer(Player playerPosition) throws IOException {
            FileHandle file = Gdx.files.local("player.dat");
            OutputStream out = null;
            try {
                    file.writeBytes(serialize(playerPosition.getPosition()), false);
            }catch(Exception ex){
                    System.out.println(ex.toString());
            }finally{
                    if(out != null) try{out.close();} catch (Exception ex){}
            }
           
            System.out.println("Saving Player");
    }
   
    public static Vector2 readPlayer() throws IOException, ClassNotFoundException {
            Vector2 playerPosition = null;
            FileHandle file = Gdx.files.local("player.dat");
            playerPosition = (Vector2) deserialize(file.readBytes());
            return playerPosition;
    }
       
        private static byte[] serialize(Object obj) throws IOException {
                ByteArrayOutputStream b = new ByteArrayOutputStream();
                ObjectOutputStream o = new ObjectOutputStream(b);
                o.writeObject(obj);
                return b.toByteArray();
        }
       
        public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
                ByteArrayInputStream b = new ByteArrayInputStream(bytes);
                ObjectInputStream o = new ObjectInputStream(b);
                return o.readObject();
        }
       
        public Vector2 getPosition(){
                return position;
        }
       
        public void setPosition(Vector2 position){
                this.position = position;
        }
        
        public TextureRegion getCurrentFrame() {
			return currentFrame;
		}

		public void setCurrentFrame(TextureRegion currentFrame) {
			this.currentFrame = currentFrame;
		}

		public Rectangle getBounds() {
			return bounds;
		}

		public void setBounds(Rectangle bounds) {
			this.bounds = bounds;
		}

		
        
}
