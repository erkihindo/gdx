package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
	
public class Tree {
	
	
	Texture tree;
	Vector2 position;
	Vector2 size;
	Rectangle bounds;
	public Tree(Vector2 position, Vector2 size){
		this.position = position;
		this.size =size;
		bounds = new Rectangle(position.x, position.y, size.x, size.y);
		tree = new Texture("tree.png");
	}
	public void update() {
		bounds.set(position.x, position.y, size.x, size.y);
	}
	
	public void draw(SpriteBatch batch) {
		batch.draw(tree, position.x, position.y, size.x,size.y);
	}
	public Rectangle getBounds() {
		return bounds;
	}
	public void setBounds(Rectangle bounds) {
		this.bounds = bounds;
	}
	
}
