package com.mygdx.game;

import java.io.IOException;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class MyGdxGame extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;
	Vector2 pos;
	Vector2 destinationPosition;
	Player player;
	Tree tree;
	ShapeRenderer sr;
	Stage stage;
	BitmapFont font;
	LabelStyle txtstyle;
	Label label;
	private FPSLogger fpsLogger;
	TextureAtlas btnAtl;
	TextButtonStyle btnStyle;
	TextButton btn;
	Skin skin;
	@Override
	public void create () {
		batch = new SpriteBatch();
		tree = new Tree(new Vector2(100,100), new Vector2(50,100));
		sr = new ShapeRenderer();
		stage = new Stage(new ScreenViewport());
		font = new BitmapFont(Gdx.files.internal("MV_Boli.fnt"));
		txtstyle= new LabelStyle(font, Color.BLACK);
		label = new Label("Insert text here", txtstyle);
		 if(Gdx.files.local("player.dat").exists()){
             try {
                     player = new Player(new Vector2(Gdx.graphics.getWidth() /2, Gdx.graphics.getHeight() /2), "data/mario.jpg");
                     player.setPosition(Player.readPlayer());
             } catch (ClassNotFoundException e) {
                     e.printStackTrace();
             } catch (IOException e) {
                     e.printStackTrace();
             }
             System.out.println("File exists, reading file");
     }else{
                     player = new Player(new Vector2(Gdx.graphics.getWidth() /2, Gdx.graphics.getHeight() /2), "data/mario.jpg");
                     try {
                             Player.savePlayer(player);
                     } catch (IOException e) {
                             e.printStackTrace();
                     }
                     System.out.println("Player does not exist, creating and saving player.");
             }
		 label.setPosition(250, 250);
    stage.addActor(label);
    
    skin = new Skin();
    btnAtl = new TextureAtlas("buttons/button.pack");
    skin.addRegions(btnAtl);
    btnStyle = new TextButtonStyle();
    btnStyle.up = skin.getDrawable("button");
    btnStyle.over = skin.getDrawable("buttonpressed");
    btnStyle.down = skin.getDrawable("buttonpressed");
    btnStyle.font = font;
    
    
    btn = new TextButton("test", btnStyle);
    stage.addActor(btn);
    Gdx.input.setInputProcessor(stage);
     player.loadContent();
	fpsLogger = new FPSLogger();
		
	
	btn.addListener(new InputListener() {
		public boolean touchDown(com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y, int pointer, int button) {
			System.out.println("clicked test");
			return true;
		};
	
	});
	}
	
	@Override
    public void dispose() {
            try {
                    Player.savePlayer(player);
            } catch (IOException e) {
                    e.printStackTrace();
            }
    }

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		/*
		if(Gdx.input.isKeyPressed(Keys.W)) {
			pos.y += 1f;
		}
		if(Gdx.input.isKeyPressed(Keys.A)) {
			pos.x -= 1f;
		}
		if(Gdx.input.isKeyPressed(Keys.S)) {
			pos.y -= 1f;
		}
		if(Gdx.input.isKeyPressed(Keys.D)) {
			pos.x += 1f;
		}*/
		  
		player.update();
		tree.update();
		if(player.getBounds().overlaps(tree.getBounds())) {	
			
		}
		
		sr.begin(ShapeType.Line);
		sr.setColor(Color.BLUE);
		sr.rect(player.getPosition().x, player.getPosition().y, player.getCurrentFrame().getRegionWidth(), player.getCurrentFrame().getRegionHeight());
		sr.end();
		stage.draw();
		batch.begin();
		player.loadContent();
		
		player.draw(batch);
		tree.draw(batch);
		
		//batch.draw(player.getTexture(), player.getPosition().x, player.getPosition().y);
		
		batch.end();
		fpsLogger.log();
	}
}
